<?php

function connect($id){
    $_SESSION["userLog"] = $id;
    header("Location: ./index.php");
    exit();
}

function isUserConnected(){
    $res = FALSE;
    if( isset($_SESSION["userLog"]) ){
        if( $_SESSION["userLog"] != ""){
            $res = TRUE;
        }
    }
    return $res;
}

function getUsrId(): int{
    return $_SESSION["userLog"];
}

function disconnect(){
    $_SESSION["userLog"] = "";
    $params="";
    session_unset();    
    header("Location: ./index.php");
    exit();
}

session_start();



?>
