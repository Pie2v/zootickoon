<code><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css"/>
    <title>Zoo de Rueil</title>
  </head>
  <body>
    <header>
      <h1>Zoo de Rueil Malmaison</h1>
      <nav>
        <ul>
          <li><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/">Page d'acceuil</a></li>
          <li><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/afrique/">Afrique</a></li>
          <li><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/asie/">Asie</a></li>
          <li><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/les-animaux-de-la-ferme/">Ferme</a></li>
          <li><a href="http://zoorueilmalmaison.alwaysdata.net/formTicket.html">Signaler un problème</a></li>
          <?php 
            include("connection.php");
            if(isUserConnected()){
              echo("<li><a href=\"http://zoorueilmalmaison.alwaysdata.net/disconnect.php\">Déconnection</a></li>");
              echo("<li><a href=\"http://zoorueilmalmaison.alwaysdata.net/afficherTickets.php\">Gestion des tickets</a></li>");
            }else{
              echo("<li><a href=\"http://zoorueilmalmaison.alwaysdata.net/authentification.html\">Connection</a></li>");
              echo("<li><a href=\"http://zoorueilmalmaison.alwaysdata.net/inscription.html\">Inscription</a></li>");
            }

          ?>
        </ul>
      </nav>
  </header>
    <h2>Current sections</h2>
    <section>
      <h3><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/afrique/">Afrique</a></h3>
      <p>Dans cette partie du Zoo, on peut voir des annimeaux venants d'Afrique: il y a des giraphes des lions, et bien d'autres!</p>
      <p>Il y a actuellement 124 annimeaux et il peut y en avoir au plus 153</p>
      <p>Le code est de cette partie est 1</p>
      <p>La surface est de 10 hectars</p>
      <?php 
      $heure= date('H');
      if($heure < 12){
        echo'<img src="images/lion.jpg" alt="image Afrique">';
      }else if($heure == 12){
        echo'<img src="images/elephant.jpg" alt="image Afrique">';
      }elseif($heure > 12){
        echo'<img src="images/girafe.jpg" alt="image Afrique">';
      }
       ?>
    </section>
    <section>
      <h3><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/asie/">Asie</a></h3>
      <p>Dans cette partie du Zoo, on peut voir des annimeaux venants d'Asie: il y a des éléphants des paons, et bien d'autres!</p>
      <p>Il y a actuellement 87 annimeaux et il peut y en avoir au plus 102</p>
      <p>Le code est de cette partie est 2</p>
      <p>La surface est de 8 hectars</p>
      <img src="images/paon.jpg" alt="image Asie">
    </section>
    <section>
      <h3><a href="http://zoorueilmalmaison.alwaysdata.net/wordpress/index.php/les-animaux-de-la-ferme/">Ferme</a></h3>
      <p>Dans cette partie du Zoo, on peut voir des annimeaux de la ferme: il y a des chèvres des ânes, et bien d'autres!</p>
      <p>Il y a actuellement 87 annimeaux et il peut y en avoir au plus 212</p>
      <p>Le code est de cette partie est 3</p>
      <p>La surface est de 15 hectars</p>
      <img src="images/chevre.jpg" alt="image de la ferme">

    </section>

  </body>
</html></code>